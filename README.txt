
=============================== Software Requirement ==========================
1. Sebelum menjalankan aplikasi pastikan mempunyai Adobe Reader versi XI atau keatas.
2. Pastikan drive Local Disk anda letter nya adalah D:\\
=============================== Mengganti Local Database ======================
1. Pertama buka Antrian.sln
2. Lalu buka Koneksi.cs di dalam folder KoneksiDB
3. Ubah lokasi dari AttachDbFilename = ke file Antrian_dat.mdf yang sudah di berikan
4. untuk path nya gunakan \\ atau double slash untuk menggantikan \ karena C# tidak dapat membaca \ sebagai path
5. Lalu jalankan aplikasi dengan menekan start di Visual Studio	
============================== Penggantian lokasi write file pdf antrian =======
1. Jika didalam pc atau komputer anda terdapat direktori D: maka dapat skip langkah ini
2. Jika didalam pc atau komputer anda tidak terdapat direktori D: maka,
3. buka Antrian.sln lalu buka CetakModel.cs pada folder Model
4. Lalu pada line 30 pada "D:\\Antrian.pdf" ganti letter D: ke partisi yang ada di pc atau komputer anda (E: atau F: contoh E:\\Antrian.pdf)
(PENTING JANGAN GANTI KE C KARENA APLIKASI TIDAK DAPAT MEWRITE PADA DRIVE C) 
5. Lalu buka Form1.cs pada folder View lalu tekan F7 atau klik kanan pada window design lalu view code
6. Pada line 21 Ubahlah Letter D: di "D:\Antrian.pdf" menjadi letter yang sama pada langkah ke 4 (Contoh :  "E:\Antrian.pdf")
(Catatan : \ disini tidak usah diganti menjadi \\ )
7. Lalu jalankan aplikasi dengan menekan start di Visual Studio				             
=============================================================================
============================== Lokasi file exe  =============================
Lokasi file exe berada di folder Antrian\Antrian\bin\Debug\
bernama Antrian.exe

=============================== Login Staff User Pass ===========================
username = 1111
password = rama
==================
=============================================================================

=============================== Login Akses Admin User Pass ====================
username = Admin
password = Admin
=============================================================================